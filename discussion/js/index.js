/*
Mini activity
How do we display the following tasks in the console?

drink html
eat javascript
inhale css
bake bootstrap

send a screenshot of the output in the Hangouts
*/
let task1 = "drink html"
let task2 ="eat javascript"
let task3 ="inhale css"
let task4 ="bake bootstrap"
console.log(task1)
console.log(task2)
console.log(task3)
console.log(task4)


/*
Arrays are used to store multiple related data inside a single variable
They are declared through the square brackets ([]), "Array Literals"
Array are constructed if there is a need to manipulate the related values inside the variable

Number of elements in an array corresponds to the total amount of the elements
The position of each element is called index. Calling an element through its index is determined by using the syntax:
	<arrayName>[index]

	Determining the index of the element:
	-start counting from 0
	-total number of elements - 1
*/

	// try for array of athmetics
/*	let number = [1+1, 2-2]
	console.log(number)*/

// total number of elements = 4
let tasks = ["drink html", "eat javascript", "inhale css", "bake bootstrap"]
console.log(tasks)
// to display the first element
console.log(tasks[0])

// to display the last element
let indexOfLastElement = tasks.length - 1
console.log(tasks[indexOfLastElement])

// to display the element in the middle
let indexOfElement = tasks.length - 2
console.log(tasks[indexOfElement])

// -----------------------------------------------------------------------
// ARRAY MANIPULATION
// setting the array
let numbers = [ "one", "two", "three", "four" ]
console.log(numbers)

// adding element 
// Push Method
	// used to add elements at the end of an array
	/*
		Syntax 
		arrayName.push(element)
	*/
	numbers.push("five")
	console.log(numbers)

function pushMethod(element){
	numbers.push(element)
}
pushMethod("six")
pushMethod("seven")
pushMethod("eight")
console.log(numbers)

// -------------------------------------------------------------
// adding an element to be removed
pushMethod("CrushAkoNgCrushKo")
console.log(numbers)

// remove an element in an array
// Pop method
	// removes an element at the end of an array
	 numbers.pop()
	 // adding parameters would be useless since the pop command will only delete the last element
	 // numbers.pop("hugot")
	 console.log(numbers)

	 function popMethod(){
	 	numbers.pop()
	 }
	 popMethod()
	 console.log(numbers)

// -------------------------------------------------------------
// adding an element at the beginning of an array
// Unshift Method
	// adds an element at start of an array
	numbers.unshift("zero")
	console.log(numbers)

	function unshiftMethod(element){
		numbers.unshift(element)
	}
	unshiftMethod("jm pogi")
	unshiftMethod("hugot")
	unshiftMethod("sanaol")
	unshiftMethod("ditoNaKo")
	console.log(numbers)

// -------------------------------------------------------------
// Shift Method
	// removes an element at the beginning of an array
	 numbers.shift()
	// adding parameters would be useless since the shift command will only delete the first element
	// numbers.shift("hugot")
	console.log(numbers)

	function shiftMethod(){
		numbers.shift()
	}
	shiftMethod()
	console.log(numbers)

	// ------------------------------------------------------------------
let numbs = [ 15, 10, 31, 24, 30 ]

// Sort Method
	// arranges the order of the elements in the array
	/*
		return a - b 
			signifies that the elements have to be rearranged in an ascending order
	*/
		// ascending order
		numbs.sort(
				function (a, b){
					return a - b
				}
			)
console.log(numbs)
		// descending order
		numbs.sort(
				function (a, b){
					return b - a
				}
			)
console.log(numbs)
// Reverse Method
	// reverses the order of the elements of the last array
	numbs.reverse()
	console.log(numbs)

	numbs.reverse()
	console.log(numbs)


// Splice Method
	// it directly manipulates the array
	// it returns the omitted elements
/*
	first parameter determines the index on which the omission will start
	second parameter determines how many elements are to be omitted from the first parameter
	third element determines the replacement for the omitted element if there is a need to do so
*/
// only one parameter
	// will delete all of the elements on the right of the specified index
/*let nums = numbs.splice(1)
console.log(numbs)
console.log(nums)*/

/*let nums = numbs.splice(3, 1)
console.log(numbs)
console.log(nums)
*/

let nums = numbs.splice(3, 1, 11, 9, 1)
console.log(numbs)
console.log(nums)

//Slice Method
	//it cannot manipulate the original array.

/*
	first parameter - the index where the copying will start, going to the right
	second parameter - the number of elements to be copied (counting starts at the first element, not the index) 
*/


	// let slicedNumbs = numbs.slice(1)
	let slicedNumbs = numbs.slice(1, 6)
	console.log(slicedNumbs)
	console.log(numbs)

// -----------------------------
// Concat
	// merge two or more arrays
	console.log(numbers)
	console.log(numbs)
	let animals = [ "snake", "chimp", "dog", "cat" ]
	console.log(animals)
/*
	parameters - determines the arrays to be merged with the "numbers" array
*/
	let newConcat = numbers.concat(numbs, animals)
	console.log(newConcat)
// the concatinated array can be merged again to another array
	let animals2 = [ "bird", "dinosaur" ]
	let concat2 = newConcat.concat(animals2)
	console.log(concat2)

/*
	Using sort to arrange the elements inside the array
	concat2.sort(
			function (a, b){
				return b - a
			}
		)
	console.log(concat2)*/


// Join Method
	// merges the elements inside the array
	// transforms the data type of the array into strings
	/*
		no parameter - elements are separated by comma
		parameters - determines the means of separating the elements
	*/
let meal = [ "rice", "lechon kawali", "Coke" ]
console.log(meal)

let newJoin = meal.join()
console.log(newJoin)

newJoin = meal.join("")
console.log(newJoin)

newJoin = meal.join(" ")
console.log(newJoin)

newJoin = meal.join("-")
console.log(newJoin)

//Accessors

let countries = [ "PKT", "US", "PH", "NZ", "UK", "AU", "SG", "JPN", "CA", "SK", "PH" ]
// indexOf() arrayName.indexOf(element)
	// detemines the index of the specified element/parameter
console.log(countries.indexOf("PH"))
// if the element is non-existing, it will return -1
console.log(countries.indexOf("RU"))
//lastIndexOf 
	// starts the searching for the index of the element from the end of the array.
console.log(countries.lastIndexOf("PH"))

// Reassignment of the elements and index
countries[0] = "NK"
console.log(countries)

// iterators
// forEach (cb())
	let days = [ "mon", "tue", "wed", "thu", "fri", "sat", "sun" ]
	days.forEach(
			function(element){
				console.log(element)
			}
		)

//Map method (cb())
 let mapDays = days.map(
 		function(day){
 			return `${day} is the of the week`
 		}
 	)
	console.log(mapDays)
	console.log(days)

/*
Mini Activity

Using forEach method, add each element of days array in an empty array called days2
*/
let days2 = [];
console.log(days2)

days.forEach(function(day){
	days2.push(day)
})

console.log(days2)

// every method checks if every element meets the condition set
let numberA = [1, 2, 3, 4, 5]

let allValid = numberA.every(e => e < 3)

console.log(allValid)

// some method parang OR

let someValid = numberA.some( e => e<2)

console.log(someValid)

// filter method
let filtered = numberA.filter(e => e < 3)
console.log(filtered)

let fil = []
numberA.forEach(e => e<3 ? fil.push(e) : false)
console.log(fil)


let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor', 'All']
let filteredProduct = products.filter(e => e.toLowerCase().includes('mon'))
console.log(filteredProduct)

let x = []
let y =[]
c = ''


function nextChar(c, i) {
    return String.fromCharCode(c.charCodeAt(0) + i);
}
nextChar('a');

for(let i = 0; i <= 8; i++) {
	// x.push(nextChar('a', i))
	for(let j = 1; j <= 8; j++) {
		x.push(nextChar('a', i)+j)
	}
	y.push(x)	
}

// console.log(x)
console.log(y)

// function nextChar(c) {
//     return String.fromCharCode(c.charCodeAt(0) + 1);
// }
// nextChar('a');